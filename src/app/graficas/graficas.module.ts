import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GraficasRoutingModule } from './graficas-routing.module';
import { BarrasComponent } from './paginas/barras/barras.component';
import { BarrasDobleComponent } from './paginas/barras-doble/barras-doble.component';
import { DonaHttpComponent } from './paginas/dona-http/dona-http.component';
import { DonaComponent } from './paginas/dona/dona.component';
import { GraficaBarraComponent } from './componentes/grafica-barra/grafica-barra.component';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
    BarrasComponent,
    BarrasDobleComponent,
    DonaHttpComponent,
    DonaComponent,
    GraficaBarraComponent
  ],
  imports: [
    CommonModule,
    GraficasRoutingModule,
    ChartsModule
  ]
})
export class GraficasModule { }
