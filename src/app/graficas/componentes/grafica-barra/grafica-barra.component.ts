import { Component, Input, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-grafica-barra',
  templateUrl: './grafica-barra.component.html',
  styles: [
  ]
})
export class GraficaBarraComponent implements OnInit {
  @Input() horizontal: boolean = false
  public barChartOptions: ChartOptions = {
    responsive: true,

  };
  @Input() barChartLabels: Label[] = [
    //'2020', '2021', '2022', '2023', '2024', '2025', '2026'
  ];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  @Input() barChartData: ChartDataSets[] = [
    // { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A', backgroundColor: '#E2F50C', hoverBackgroundColor: '#E2F50C' },
    // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B', backgroundColor: '#FF6D0D', hoverBackgroundColor: '#FF6D0D'},
    // { data: [53, 21, 40, 90, 15, 81, 70], label: 'Series C', backgroundColor: '#B417E8', hoverBackgroundColor: '#B417E8' }
  ];
  constructor() {
    this.horizontal
   }

  ngOnInit(): void {
    if(this.horizontal){
      this.barChartType ='horizontalBar'
    }
    
  }

}
